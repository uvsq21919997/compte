package compte;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

public class CompteBancaireTest {

	CompteBancaire compteBancaire1;
	CompteBancaire compteBancaire2;
	BigDecimal soldeInitial = new BigDecimal(300);
	BigDecimal soldeInitialNegatif = new BigDecimal(-300);

	@Test
	public void testInitialisationDeCompte() {
		compteBancaire1 = new CompteBancaire(new BigDecimal(300));
		BigDecimal soldeInitial = compteBancaire1.getSolde();
		assertEquals(this.soldeInitial, soldeInitial);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testInitialisationAvecUnSoldeNegatif() {
		compteBancaire1 = new CompteBancaire(soldeInitialNegatif);
		// assertEquals(this.soldeInitial, compteBancaire1.getSolde());
	}

	@Test
	public void testCrediterLeCompte() {
		compteBancaire1 = new CompteBancaire(new BigDecimal(300));
		compteBancaire1.crediterCompte(new BigDecimal(100));
		assertEquals(new BigDecimal(400), compteBancaire1.getSolde());

	}

	@Test(expected = IllegalArgumentException.class)
	public void testCrediterLeCompteAvecUnSoldeNegatif() {
		compteBancaire1 = new CompteBancaire(new BigDecimal(300));
		compteBancaire1.crediterCompte(new BigDecimal(-100));
		// assertEquals(new BigDecimal(200), compteBancaire1.getSolde());
	}

	@Test
	public void testDebiterLeCompte() {
		compteBancaire1 = new CompteBancaire(new BigDecimal(300));
		compteBancaire1.debiterCompte(new BigDecimal(100));
		assertEquals(new BigDecimal(200), compteBancaire1.getSolde());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDebiterLeCompteAvecUnDebitNegatif() {
		compteBancaire1 = new CompteBancaire(new BigDecimal(300));
		compteBancaire1.debiterCompte(new BigDecimal(-100));
		// assertEquals(new BigDecimal(200), compteBancaire1.getSolde());

	}

	@Test(expected = IllegalArgumentException.class)
	public void testDebiterLeCompteAvecUnDebitSuperieur() {
		compteBancaire1 = new CompteBancaire(new BigDecimal(300));
		compteBancaire1.debiterCompte(new BigDecimal(400));
		assertEquals(new BigDecimal(-100), compteBancaire1.getSolde());

	}

	@Test
	public void testVirementAvecMontantPositif() {
		compteBancaire1 = new CompteBancaire(new BigDecimal(300));
		compteBancaire2 = new CompteBancaire(new BigDecimal(200));
		compteBancaire1.virement(compteBancaire2, new BigDecimal(100));
		assertEquals(new BigDecimal(300), compteBancaire2.getSolde());
		assertEquals(new BigDecimal(200), compteBancaire1.getSolde());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testVirementAvecMontantNegatif() {
		compteBancaire1 = new CompteBancaire(new BigDecimal(300));
		compteBancaire2 = new CompteBancaire(new BigDecimal(200));
		compteBancaire1.virement(compteBancaire2, new BigDecimal(-100));
		// assertEquals(new BigDecimal(300), compteBancaire2.getSolde());
		// assertEquals(new BigDecimal(200), compteBancaire1.getSolde());

	}

	@Test(expected = IllegalArgumentException.class)
	public void testVirementMemeCompteBancaire() {
		compteBancaire1 = new CompteBancaire(new BigDecimal(300));
		compteBancaire1.virement(compteBancaire1, new BigDecimal(100));

	}

}
