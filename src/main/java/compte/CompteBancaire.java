package compte;

import java.math.BigDecimal;

/**
 * <b>CompteBancaire est la classe qui représente un compte bancaire.</b>
 * <p>
 * Elle permet de :
 * <ul>
 * <li>Consulter, créditer et débiter un compte bancaire.</li>
 * <li>Virement vers un autre compte bancaire.</li>
 * </ul>
 * 
 * 
 * @author medish
 * @version 1.0
 */
public class CompteBancaire {
	/**
	 * Le solde initial de compte bancaire.
	 * 
	 * @see BigDecimal
	 * @see #getSolde()
	 * @see #setSolde(BigDecimal)
	 * @see #verifieSoldeNegatif(BigDecimal)
	 */
	private BigDecimal soldeInitial;

	/**
	 * Constructeur CompteBancaire
	 * 
	 * @param soldeInitial
	 *            Le solde initial du compte bancaire.
	 * 
	 * @see #soldeInitial
	 * @see #verifieSoldeNegatif(BigDecimal)
	 * @see #setSolde(BigDecimal)
	 */
	public CompteBancaire(BigDecimal soldeInitial) {
		verifieSoldeNegatif(soldeInitial);
		setSolde(soldeInitial);

	}

	/**
	 * Créditer un compte bancaire.
	 * 
	 * @param credit
	 *            le montant à créditer.
	 * 
	 * @see #verifieSoldeNegatif(BigDecimal)
	 */
	public void crediterCompte(BigDecimal credit) {
		verifieSoldeNegatif(credit);
		soldeInitial = soldeInitial.add(credit);

	}

	/**
	 * Débiter un compte bancaire.
	 * 
	 * @param debit
	 *            le montant à débiter.
	 * 
	 * @see #verifieSoldeNegatif(BigDecimal)
	 * @throws IllegalArgumentException
	 *             Si le débit est supérieur au solde.
	 * 
	 */
	public void debiterCompte(BigDecimal debit) {
		verifieSoldeNegatif(debit);
		if (debit.compareTo(soldeInitial) > 0)
			throw new IllegalArgumentException("Le débit " + debit + " est superieur au solde");
		soldeInitial = soldeInitial.subtract(debit);
	}

	/**
	 * Virement d'un montant vers un autre compte bancaire.
	 * 
	 * @param compteBancaire
	 *            Le compte bancaire qui reçoit le montant.
	 * 
	 * @param montant
	 *            Le montant à versé.
	 * 
	 * @see CompteBancaire
	 * @see #verifieSoldeNegatif(BigDecimal)
	 * @see #debiterCompte(BigDecimal)
	 * @see #crediterCompte(BigDecimal)
	 * 
	 * @throws IllegalArgumentException
	 *             Si virement vers le même compte.
	 */
	public void virement(CompteBancaire compteBancaire, BigDecimal montant) {
		if (this == compteBancaire)
			throw new IllegalArgumentException("Virement vers le même compte!");
		verifieSoldeNegatif(montant);
		this.debiterCompte(montant);
		compteBancaire.crediterCompte(montant);
	}

	/**
	 * Retourne le solde du compte bancaire.
	 * 
	 * @return Le solde du compte bancaire
	 */
	public BigDecimal getSolde() {
		return soldeInitial;
	}

	/**
	 * Met à jour le solde du compte bancaire
	 * 
	 * @param solde
	 *            Le nouveau solde.
	 * 
	 * 
	 */
	public void setSolde(BigDecimal solde) {
		this.soldeInitial = solde;
	}

	/**
	 * Vérifie si le solde est négatif
	 * 
	 * @param solde
	 *            Le solde à vérifié.
	 * @throws IllegalArgumentException
	 *             Si le solde est négatif
	 */
	public void verifieSoldeNegatif(BigDecimal solde) {
		if (solde.compareTo(BigDecimal.ZERO) < 0)
			throw new IllegalArgumentException("Le solde " + solde + " est négatif");

	}

	public static void main(String[] args){
		System.out.println("Hello world from CompteBancaire class");
	}
}
